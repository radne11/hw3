import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;


//http://stackoverflow.com/questions/3481828/how-to-split-a-string-in-java
//https://rosettacode.org/wiki/Parsing/RPN_calculator_algorithm#Java_2
//http://javadevnotes.com/java-string-split-tutorial-and-examples
//http://enos.itcollege.ee/~jpoial/algorithms/examples/Astack.java
//https://git.wut.ee/i231/home3/src/master/src/LongStack.java
//http://enos.itcollege.ee/~ylari/I231/Intstack.java
//https://github.com/margusja/DoubleStack/blob/master/src/DoubleStack.java
//https://bitbucket.org/margoAl/homework3/src/e52a0733a47810d471a0a60e12e04e51ea08d9cd/src/DoubleStack.java?at=master&fileviewer=file-view-default
//kasutasin kaasõpilaste abi

//author @RadneK

public class DoubleStack {
    static int luger=0;
	   
	private LinkedList<Double> inList = new LinkedList<Double>();

	public static void main(String[] argum) {
		DoubleStack m = new DoubleStack();
		System.out.println(m);
		m.push(1);
		System.out.println(m);
		m.push(3);
		System.out.println(m);
		m.push(6);
		System.out.println(m);
		m.push(2);
		System.out.println(m);
		m.op("/");
		System.out.println(m);
		m.op("*");
		System.out.println(m);
		m.op("-");
		System.out.println(m);
		double result = m.pop();
		System.out.println(m);
		System.out.println(result);
		DoubleStack copy = m;
		System.out.println(copy.equals(m));
		System.out.println(m);
		System.out.println(copy);
		try {
			copy = (DoubleStack) m.clone();
		} catch (CloneNotSupportedException e) {
		}
		System.out.println(copy.equals(m));
		System.out.println(m);
		System.out.println(copy);
		m.push(6);
		System.out.println(copy.equals(m));
		System.out.println(m);
		System.out.println(copy);
		m.pop();
		System.out.println(copy.equals(m));
		System.out.println(m);
		System.out.println(copy);
		
		String prog = "3 4 /";
		
		if (argum.length > 0) {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < argum.length; i++) {
				sb.append(argum[i]);
				sb.append(" ");
			}
			prog = sb.toString();
		}
		System.out.println(interpret("x 5"));
	}


   DoubleStack() {
	   //this (10);
   }

   
   @Override
   public Object clone() throws CloneNotSupportedException {
	   DoubleStack tmp = new DoubleStack ();
		for (int i = (inList.size() - 1); i >= 0; i--)
			tmp.push(inList.get(i));
//tmp.inList = (LinkedList<Double>) inList.clone(); teine variant
	     return tmp;
   }

   
   public boolean stEmpty() {
	   return inList.isEmpty(); //list on tyhi
	   
	   /*if (inList.size() == 0) {
         return true;
      } else {
         return false;
      }
*/
   }

   
   public void push (double a) {
	   inList.addFirst(a); //lisatakse esimeseks 
//inList.push(a);
   }

   
   public double pop() {
	   if (stEmpty())
	         throw new RuntimeException ("Loend nr "+luger+": sisestatud stringis on tyhi nimekiri, ei saa esimest elementi v2lja v6tta (pop)");
	    	    
		double top = inList.getFirst(); //annab top'ile esimese vaartuse
		inList.removeFirst(); //eemaldab selle listist
		return top; //väljastab selle
//return inList.pop();
   }

   
   public void op (String s) throws ArithmeticException  {
//enne kumbagi pop'i kontrollitakse, et ega loend tühi pole.
	   if (stEmpty())
	         throw new RuntimeException ("Loend nr "+luger+": sisestatud stringis on tyhi nimekiri, ei saa esimest elementi v2lja v6tta (op)");
	    
     double op1 = inList.pop();
     
     if (stEmpty())
         throw new RuntimeException ("Loend nr "+luger+": sisestatud stringis on tyhi nimekiri, ei saa esimest elementi v2lja v6tta (op)");
    
	 double op2 = inList.pop();

  	 if (s.equals("+")) 
		inList.push(op2 + op1);
  	 else if(s.equals("-")) 
		inList.push(op2 - op1);
  	 else if (s.equals("*")) 
		inList.push(op2 * op1);
  	 else if (s.equals("/")) {
  		 if(op1 != 0 )
		     inList.push(op2 / op1);
  		 else 
  			 throw new ArithmeticException ("Loend nr "+luger+": Sisestati '"+op2+" "+op1+" "+s+"', milles sisalduva '"+op1+"'-ga ei saa jagamist teostada!");
  	  }
	 else
		throw new RuntimeException("Loend nr "+luger+":Sisestati (op) ebasobiv m2rk '"+s+"'. Ei saa tehet teha. Sobivad m2rgid on +, -, * ja / ");
   }
  
   
   public double tos()throws RuntimeException {
	   if (stEmpty())
	         throw new RuntimeException ("Loend nr "+luger+":sisestati tyhi nimekiri, pole midagi lugeda(tos)");
	   
	  double top = inList.getFirst();
	  return top; //loetakse, kuid ei eemaldata
//return inList.getFirst().doubleValue();
   }

   
   @Override
	public boolean equals (Object o) {
	 //  DoubleStack inList2 = new DoubleStack();
	      if (inList.equals(((DoubleStack) o).inList))
	      {
	         return true;
	      }else {
	         return false;
	      }

   }

   
   @Override
	public String toString() {
	   if (stEmpty()) 
		   System.out.println("Sinu "+luger+". sisestatud stringis on tyhi nimekiri, ei saa esimest elementi v2lja v6tta (toString)"); //siin on lubatud tyhi nimekiri, sellepärast ei tee veatootlust
		StringBuffer textina = new StringBuffer();
		for (int i = inList.size() - 1; i >= 0; i--)
			textina.append(inList.get(i)); //append lisab elemendi
		return textina.toString();
	}


  public static double interpret (String pol) {
	luger++; //loeb, et mitu korda meetodit välja on kutsutud, et öelda täpselt, mitmendas sisendis oli probleem
	if (pol == null || pol.trim().length() == 0)
			throw new RuntimeException("Sisestasid "+luger+". nimekirja, mis (interpret)  oli tyhi hulk!");

    DoubleStack uusList = new DoubleStack();
    StringTokenizer jupitaja = new StringTokenizer(pol, " \t");	

	int i = 1;
	int b=0;
	int c=0;
	int loendur = jupitaja.countTokens();

	while (jupitaja.hasMoreTokens()) {
		if (loendur== 0)
			throw new RuntimeException("Sisestasid "+luger+". nimekirja, mis (interpret)  oli tyhi hulk!");
		String jupp = (String) jupitaja.nextElement();			
		if (jupp.equals("-") || jupp.equals("+") || jupp.equals("/") || jupp.equals("*")) {
			if (uusList.inList.size() < 2)
				throw new RuntimeException("Loend nr "+luger+" ('"+pol+"'): Liiga v2he elemente stackis, et teostada tehet ('"+jupp+"') .");
			if (uusList.stEmpty())
				throw new RuntimeException("Loend nr "+luger+" ('"+pol+"'): Tehe ('"+jupp+"') ei saa olla avaldise esimene element ");
			uusList.op(jupp);
			b++;
		} else{

			try{
			    double a = Double.parseDouble(jupp);
			    uusList.push(a);
			    c++;
			}
			catch(NumberFormatException nfe) {  
			    System.out.println("Sinu "+luger+". sisestatud stringis '"+pol+"'  sisalduv '"+jupp+"' ole number ega aktsepteeritud m2rk!(interpret)"); 
			   throw new RuntimeException("Sinu "+luger+". sisestatud stringis '"+pol+"'  sisalduv '"+jupp+"' ole number ega aktsepteeritud märk!(interpret)");
			} 
			if (loendur == i && i > 2) {
				throw new RuntimeException("Loend nr "+luger+" ("+pol+") ei ole tasakaalus: tokenite hulk '"+loendur+"' vs tehete arv '"+i+"' .");
			}
		}
		i++;
	
	}
	if (b+1<c) {
		throw new RuntimeException("Loend nr "+luger+" ('"+pol+"') ei ole tasakaalus: liiga vähe tehteid ("+b+") esitatud numbritearvu kohta ("+c+") .");
	}
	if (b+1>c) {
		throw new RuntimeException("Loend nr "+luger+" ('"+pol+"') ei ole tasakaalus: liiga palju tehteid ("+b+") esitatud numbritearvu kohta ("+c+") .");
	}
	if (b+1==c) {
		System.out.println("Loend nr "+luger+" ('"+pol+"'): Esitatud RPN on tasakaalus, selles on "+loendur+" tokenit, mis jagunevad "+c+" numbrit ja "+b+" tehet");
	}
	System.out.println(uusList.tos());
	return uusList.tos();

  
  }
}


